# -*- coding: utf-8 -*-

def install():
    try:
        import flask
    except ImportError:
        raise ImportError('flask does not exist, try pip install flask')
    try:
        from flask.ext.login import login_user
    except ImportError:
        raise ImportError('flask-login does not exist, try pip install flask-login')
    try:
        from sqlalchemy import desc
    except ImportError:
        raise ImportError('sqlalchemy does not exist, try pip install sqlalchemy, flask-sqlalchemy, sqlalchemy-migrate')
    try:
        from wand.image import Image
    except ImportError:
        raise ImportError('Image magic does not exist, try sudo apt-get install libmagickwand-dev')

    import sqlalchemy
    # sql_uri = SQLALCHEMY_DATABASE_URI.split('@')[0]
    sql_user = raw_input('введите имя пользователя ...')
    sql_password = raw_input('введите пароль ...')
    sql_bd = raw_input('введите название бд (enter, чтобы пропустить)...')
    engine = sqlalchemy.create_engine('mysql://' + sql_user + ':' + sql_password + '@localhost')  # connect to server
    if sql_bd == '':
        sql_bd = "blog"
    engine.execute("CREATE DATABASE " + sql_bd)  # create db
    # engine.execute("USE blog") # select new db
    file = open("config.py", 'w')
    file.write("# -*- coding: utf-8 -*-\n" +
    "CSRF_ENABLED = True\n" +
    "SQLALCHEMY_TRACK_MODIFICATIONS = True\n" +


    "SECRET_KEY = 'you-shall-not-pass'\n" +
    "import os\n" +

    "basedir = os.path.abspath(os.path.dirname(__file__))\n" +

    "SQLALCHEMY_DATABASE_URI = \'mysql://" + sql_user + ":" + sql_password + "@localhost/" + sql_bd + "\'\n"  +
    "SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')\n")
    file.close()


    from migrate.versioning import api
    from config import SQLALCHEMY_DATABASE_URI
    from config import SQLALCHEMY_MIGRATE_REPO
    from app import db
    import os.path
    db.create_all()
    if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
        api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
        api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
    else:
        api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))
