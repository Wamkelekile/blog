$(document).ready(function () {

    $("div.dislike").hover(
        function () {
            var post_id = (this).getAttribute("post_id");
            $('#likes-count-' + post_id.toString()).removeClass("neutral-active").removeClass("like-active").addClass("dislike-active");
        },
        function() {
            var post_id = (this).getAttribute("post_id");
            if ((this).getAttribute("cond") == "up"){
                $('#likes-count-' + post_id.toString()).removeClass("dislike-active").addClass("like-active");
            } else if ((this).getAttribute("cond") == "zero") {
                $('#likes-count-' + post_id.toString()).removeClass("dislike-active").addClass("neutral-active");
            } else {}

        }
    );
    $("div.like").hover(
        function () {
            var post_id = (this).getAttribute("post_id");
            $('#likes-count-' + post_id.toString()).removeClass("neutral-active").removeClass("dislike-active").addClass("like-active");
        },
        function() {
            var post_id = (this).getAttribute("post_id");
            if ((this).getAttribute("cond") == "up"){
                $('#likes-count-' + post_id.toString()).removeClass("like-active").addClass("dislike-active");
            } else if ((this).getAttribute("cond") == "zero") {
                $('#likes-count-' + post_id.toString()).removeClass("like-active").addClass("neutral-active");
            } else {}

        }
    );
    $(".dislike").click(
        function() {
            if ((this).getAttribute("user_type") == 1) {
                return alert("Для голосования необходимо войти или зарегистрироваться");
            }
            var post_id = (this).getAttribute("post_id");
            var likes_count_selector = $('#likes-count-' + post_id.toString());
            var likes_count = likes_count_selector.attr("count").toString();

            if ((this).getAttribute("cond") != "down") {
                likes_count_selector.empty();
                likes_count_selector.append(parseInt(likes_count) - 1);
                likes_count_selector.attr("count", parseInt(likes_count) - 1);

                if ((this).getAttribute("cond") == "zero") {
                    $(this).attr("cond", "down");
                    $("#like-" + post_id).attr("cond", "up");

                } else {
                    $(this).attr("cond", "zero");
                    $("#like-" + post_id).attr("cond", "zero");
                }

                $.getJSON('/_like', {
                    for_post_id: (this).getAttribute("post_id"),
                    from_user_id: (this).getAttribute("user_id"),
                    vote_type: (this).getAttribute("cond"),
                    type: (this).getAttribute("type")
                }, function (data) {});
            } else {
                alert("Вы уже проголосовали против");
            }
        }
    );
    $(".like").click(
        function() {
            if ((this).getAttribute("user_type") == 1) {
                return alert("Для голосования необходимо войти или зарегистрироваться");
            }
            var post_id = (this).getAttribute("post_id");
            var likes_count_selector = $('#likes-count-' + post_id.toString());
            var likes_count = likes_count_selector.attr("count").toString();
            if ((this).getAttribute("cond") != "down") {
                likes_count_selector.empty();
                likes_count_selector.append(parseInt(likes_count) + 1);
                likes_count_selector.attr("count", parseInt(likes_count) + 1);
                if ((this).getAttribute("cond") == "zero") {
                    $(this).attr("cond", "down");
                    $("#dislike-" + post_id).attr("cond", "up");
                } else {
                    $(this).attr("cond", "zero");
                    $("#dislike-" + post_id).attr("cond", "zero");
                }
                $.getJSON('/_like', {
                    for_post_id: (this).getAttribute("post_id"),
                    from_user_id: (this).getAttribute("user_id"),
                    vote_type: (this).getAttribute("cond"),
                    type: (this).getAttribute("type")
                }, function (data) {});
            } else {
                alert("Вы уже проголосовали за");
            }
        }
    );

    $('form#comment-form').bind('submit', function() {
          var post_id= $(this).find("input[name='post_id']").val();
          $.getJSON('/_comment', {
              post_id: post_id,
              user_id: $(this).find("input[name='user_id']").val(),
              body: encodeURI($(this).find("textarea[name='body']").val())
          }, function(data) {
              $("input#page-time").val(data.time);

              $('h4.title-bg').after(
                  '<ul>' +
                     '<li>' +
                        '<img src="/static/img/user-avatar.jpg" alt="Image" />' +
                        '<span class="comment-name">' + data.user_nickname + '&nbsp</span>' +
                        '<span class="comment-date">' + data.time + '</span>' +
                        '<div class="comment-content">' + data.body.replace( /</g, "\&lt;").replace(/>/, "\&gt;") + '</div>' +
                     '</li>' +
                  '</ul>'
              );
              jQuery("#comment-form").trigger('reset');
          });
        return false;
    });
    if ($("input#page-type").val() == "post") {
        setInterval(function new_comments() {
            var comment_form_selector = $('#comment-form');
            $.getJSON('/_new_comments', {
                post_id: comment_form_selector.find("input[name='post_id']").val(),
                time: $("input#page-time").val()

            }, function (data) {
                $("#page-time").val(data.time);
                for (var i = 0; i < data.comments.length; ++i) { //[body, timestamp, nickname, avatar]
                    $('h4.title-bg').after(
                        '<ul>' +
                        '<li>' +
                        '<img src="/static/img/user-avatar.jpg" alt="Image" />' +
                        '<span class="comment-name">' + data.comments[i][2].replace( /</g, "\&lt;").replace(/>/, "\&gt;") + '&nbsp</span>' +
                        '<span class="comment-date">' + data.comments[i][1].replace( /</g, "\&lt;").replace(/>/, "\&gt;") + '</span>' +
                        '<div class="comment-content">' + data.comments[i][0].replace( /</g, "\&lt;").replace(/>/, "\&gt;") + '</div>' +
                        '</li>' +
                        '</ul>'
                    );

                }
                //alert(data.time);

            });
            return false;

        }, 10000)
    }

    $('button#follow').bind('click', function() {
          var mark;
          mark = +document.getElementById('is_follower').value;
          if (mark == '1') {
              $('#is_follower').val('0');
          } else {
              $('#is_follower').val('1');
          }
        $.getJSON('/_follow_user', {
            is_follower: $('input[name="is_follower"]').val(),
            view_user_id: $('input[name="user-id"]').val(),
            user_id: $('input[name="owner-id"]').val()
        }, function(data) {
            if (data.result) {
                $("button#follow").text("OUT");
            } else {
                $("button#follow").text("JOIN");
            }
        });
        return false;
    });

    $(".rights-button").click(
        function() {
            var member_id = (this).getAttribute("member_id");
            var button_selector = $('#prev-' + member_id.toString());
            var mark = button_selector.attr("value");
            if (mark == '1') {
                button_selector.val('0');
            } else {
                button_selector.val('1');
            }

        $.getJSON('/_send_rights', {
            member_id: button_selector.attr("member_id"),
            is_admin: button_selector.val(),
            user_id: $('input[name="owner-id"]').val()
        }, function(data) {
            if (data.result) {
                button_selector.text("ADMIN");
            } else {
                button_selector.text("USER");
            }
        });
        return false;
        }
    );

    $(".ban-button").click(
        function() {
            var member_id = (this).getAttribute("member_id");
            var button_selector = $('#ban-' + member_id.toString());
            var mark = button_selector.attr("value");
            if (mark == '1') {
                button_selector.val('0');
            } else {
                button_selector.val('1');
            }

        $.getJSON('/_send_ban', {
            member_id: button_selector.attr("member_id"),
            is_ban: button_selector.val(),
            user_id: $('input[name="owner-id"]').val()
        }, function(data) {
            if (data.result) {
                button_selector.text("BANNED");
            } else {
                button_selector.text("BAN");
            }
        });
        return false;
        }
    );

    $(".btn-delete").click(
        function() {
            var post_id = (this).getAttribute("post_id");
            var article_selector = $('#art-' + post_id.toString());
            $.getJSON('/_delete_post', {
                post_id: post_id
            }, function(data) {
                article_selector.empty();
            });
            return false;
        }
    );
    //setInterval(function new_likes() {
    //    var comment_form_selector = $('#comment-form');
    //    $.getJSON('/_new_likes', {
    //        time: $("input#page-time").val(),
    //        user_id : $("input#user-id").val()
    //    }, function (data) {
    //        for (var i = 0; i < data.likes.length; ++i) { //[post_id, like type, user liked or didn't]
    //
    //        }
    //        //alert(data.time);
    //
    //    });
    //    return false;
    //
    //}, 10000)




});