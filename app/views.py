# coding=utf-8
from flask import render_template, redirect, session, url_for, request, g, jsonify
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, lm
from app import db
import urllib
from sqlalchemy import desc, update
from wand.image import Image
from datetime import datetime
import random
import json

from app.models import User, Post, User_login, Comment, User_follower, Blog_admin, Post_like


@app.route('/')
@app.route('/index/<int:page>')
def index(page=1):
    user = g.user
    posts = Post.query.order_by(desc(Post.rating)).paginate(page, 10, False).items
    posts_full = []
    guest = 0 if user is not None and user.is_authenticated else 1
    for post in posts:
        liked = 0
        if not guest and Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first() is not None:
            liked = Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first().type
        posts_full.append([post, User.query.filter_by(id=post.user_id).first(), liked])  # [post, user, like type]
    return render_template("index.htm",
                           user=user,
                           current_user=current_user,
                           posts=posts_full,
                           guest=guest,
                           time=str(datetime.utcnow()))


@app.route('/post/<int:post_id>', methods=["GET", "POST"])
@login_required
def full_post(post_id):
    user = g.user
    post = Post.query.filter_by(id=post_id).first()
    owner = User.query.filter_by(id=post.subuser_id).first()
    liked = Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first().type if \
        Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first() is not None else 0
    print "LIKED", liked
    comments = Comment.query.order_by(desc(Comment.timestamp)).filter_by(post_id=post.id).paginate(1, 10, False).items
    full_comments = []  # [comment, user]
    for comment in comments:
        full_comments.append([comment, User.query.filter_by(id=comment.user_id).first()])
    return render_template('post.htm', post=post, user=user, owner=owner, liked=liked, comments=full_comments,
                           time=str(datetime.utcnow()))


@app.route("/_comment")
def comment():
    post_id = request.args.get("post_id", 0, type=int)
    user_id = request.args.get("user_id", 0, type=int)
    body = urllib.unquote(request.args.get("body", 0, type=str)).decode('utf8')
    new_comment = Comment(body=body, user_id=user_id, post_id=post_id)
    print new_comment.body
    db.session.add(new_comment)
    db.session.commit()
    user = User.query.filter_by(id=user_id).first()

    return jsonify(time=str(datetime.utcnow()),
                   body=body,
                   user_nickname=user.nickname,
                   user_avatar_mini=user.avatar_mini)


# SetTImer for new_comments
@app.route('/_new_comments')
@login_required
def new_comments():
    time = datetime.strptime(request.args.get("time", 0, type=str), "%Y-%m-%d %H:%M:%S.%f")
    post_id = request.args.get("post_id", 0, type=int)
    print time, post_id
    full_comments = []  # [comment, user]
    comments = Comment.query.order_by(desc(Comment.timestamp)).filter(Comment.timestamp > time).all()
    for comment in comments:
        user = User.query.filter_by(id=comment.user_id).first()
        print user.nickname
        full_comments.append([comment.body, comment.timestamp, user.nickname, user.avatar_mini])
    full_comments.reverse()
    # print ("time: ", datetime.datetime.strptime(time, "%Y-%m-%d %H:%M:%S.%f"))
    return jsonify(time=str(datetime.utcnow()),
                   post_id=post_id,
                   comments=full_comments)

# @app.route('/_new_likes')
# @login_required
# def new_likes():
#     time = datetime.strptime(request.args.get("time", 0, type=str), "%Y-%m-%d %H:%M:%S.%f")
#     user_id = request.args.get("user_id", 0, type=int)
#     full_likes = [] # [post_id, like type, user like or didn't]
#     likes = Post_like.query.order_by(desc(Post_like.timestamp)).filter(Post_like.timestamp > time).all()
#     for like in likes:
#         full_likes.append([like.for_post_id, like.type, 1 if user_id == like.from_user_id else 0])
#     return jsonify(time=str(datetime.utcnow()),
#                    likes=full_likes)


@app.route('/login', methods=["GET", "POST"])
def login():
    if request.method == "POST":
        login = request.form["inputLogin"]
        password = request.form["inputPassword"]
        if 'remember-me' in request.form.values():
            remember_me = True
        else:
            remember_me = False
        user_login = User_login.query.filter_by(email=login).first()
        if not user_login:
            return redirect(url_for('login'))

        true_password = User_login.query.filter_by(email=login).first().password
        if password == true_password:
            user = User.query.filter_by(id=user_login.user_id).first()
            login_user(user, remember=remember_me)
            return redirect(url_for('user_page', nickname=user.nickname))
        else:
            return redirect(url_for('login'))
    return render_template('login.html')


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    capcha = {1: u'один', 2: u'два', 3: u'три', 4: u'четыре', 5: u'пять',
               6: u'шесть', 7: u'семь', 8: u'восемь', 9: u'девять', 10: u'десять'}
    number_one = random.randint(1, 10)
    number_two = random.randint(1, 10)
    result = number_one + number_two
    print result
    messages = "sdsd"
    try:
        messages = request.args['messages']
    except KeyError:
        messages = "0"
    if request.method == "POST":
        email = request.form["inputEmail"]
        nickname = email.rsplit('@', 1)[0]  # duble nickname
        password = request.form["inputPassword"]
        curr_password = request.form["inputCurr_Password"]
        is_bot = True
        agree = False
        if "agree" in request.form.values():
            agree = True
        # print result
        if request.form["inputCapcha"] == request.form["inputResult"]:
            is_bot = False
        if password == curr_password and agree and not is_bot:
            if User_login.query.filter_by(email=email).first():
                return render_template("sign_up.html")
            new_user = User(nickname=nickname, full_name='', birthday='', vk='', fb='', instagram='',
                            gender='', avatar='', description='',
                            type=0, access_rating=0, owner_id=0)
            db.session.add(new_user)
            db.session.commit()
            user = User.query.filter_by(nickname=nickname).first()
            login_info = User_login(email=email, password=password, user_id=user.id)
            db.session.add(login_info)
            db.session.commit()
            login_user(user, remember=False)
            return redirect(url_for('user_page', nickname=user.nickname))
        else:
            messages = json.dumps({"capcha": "1"})
            session["messages"] = messages
            return redirect(url_for('registration', messages=messages))#render_template('sign_up.html')
    capcha_error = 1
    if messages == "0":
        capcha_error = 0

    return render_template('sign_up.html',
                           number_one=capcha[number_one],
                           number_two=capcha[number_two],
                           result=result,
                           capcha_error=capcha_error,
                           messages=json.loads(messages))


@app.route('/new_blog', methods=['GET', 'POST'])
@login_required
def create_blog():
    user = g.user
    if request.method == "POST":
        title = request.form["inputTitle"]
        description = request.form["inputDescription"]
        new_blog = User(nickname=title, full_name='', birthday='', vk='', fb='', instagram='',
                        gender='', avatar='', description=description, type=1, access_rating=0, owner_id=user.id)
        db.session.add(new_blog)
        db.session.commit()
        blog = User.query.filter_by(nickname=title).first()
        return redirect(url_for('user_page', nickname=blog.nickname))
    return render_template('blogs.html')

# TODO запретить доступ к постам если не фолловишь блог или забанен

@app.route('/user/<nickname>', methods=['GET', 'POST'])
@app.route('/user/<nickname>/<int:page>', methods=['GET', 'POST'])
@login_required
def user_page(nickname, page=1):
    user = g.user
    owner = User.query.filter_by(nickname=nickname).first()
    if owner is None:
        return render_template("404.html")
    rights = ''
    if int(user.id) == int(owner.id):
        rights = "me"
    elif int(user.id) == int(owner.owner_id):
        rights = "blogowner"
    elif int(owner.owner_id) == 0:
        rights = "user"
    else:
        follower_info = User_follower.query.filter_by(following_id=owner.id, follower_id=user.id).first()
        if follower_info is None:
            rights = "nobody"
        elif follower_info.banned == 1:
            rights = "ban"
        elif follower_info.rights == 1:  # TODO вывести список пользователей админов и прочих
            rights = "blogadmin"
        elif follower_info.rights == 2:
            rights = "bloguser"

    owners = []
    admins = []
    members = []
    members_ban = []
    if owner.type == 1:
        owners.append(User.query.filter_by(id=owner.owner_id).first())
        admin_ids = [blog_a.follower_id for blog_a in User_follower.query.filter_by(following_id=owner.id, rights=1)]
        member_ids = [blog_m.follower_id for blog_m in User_follower.query.filter_by(following_id=owner.id, rights=2, banned=0)]
        member_ids_ban = [blog_m.follower_id for blog_m in User_follower.query.filter_by(following_id=owner.id, rights=2, banned=1)]
        # print member_ids, admin_ids
        for blog_a_id in admin_ids:
            admins.append(User.query.filter_by(id=blog_a_id).first())
        for blog_m_id in member_ids:
            members.append(User.query.filter_by(id=blog_m_id).first())
        for blog_m_id in member_ids_ban:
            members_ban.append(User.query.filter_by(id=blog_m_id).first())
    posts_full = []
    posts = Post.query.order_by(desc(Post.timestamp)).filter_by(user_id=owner.id).paginate(page, 10, False).items
    for post in posts:
        like = Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first()
        # if like is not None:
        #     liked = like.type
        # else:
        #     liked = 0

        liked = Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first().type if \
            Post_like.query.filter_by(for_post_id=post.id, from_user_id=user.id).first() is not None else 0
        posts_full.append([post, liked])  # [post, likes count, liked, not or disliked]

        print (len(Post_like.query.filter_by(for_post_id=post.id, type=1).all()) -
               len(Post_like.query.filter_by(for_post_id=post.id, type=-1).all()))

    if request.method == "POST":
        body = request.form["inputBody"]
        # if "blog" in request.form.values():
        new_post = Post(body=body, user_id=owner.id, image='', subuser_id=owner.id)
        # else:
        #     new_post = Post(body=body, user_id=owner.id, image='', subuser_id=user.id)
        #
        print body
        print new_post.body
        db.session.add(new_post)
        db.session.commit()
        # image = request.files["inputImage"]
        # if image.filename == '':
        #     pass
        # else:
        #     image_type = image.filename.rsplit('.', 1)[1]
        #     if image_type in ["jpeg", "jpg", "png"]:
        #         image_href = "/static/images/" + str(new_post.id) + "post." + image_type
        #         new_post.image = image_href
        #         db.session.commit()
        #     else:  # errors
        #         return redirect(url_for("/"))
        #     # do with image using imagemagic
        #     image.save('app' + image_href)
        #     with Image(filename='app' + image_href) as img:
        #         img.resize(220, 270)
        #         img.save(filename='app' + image_href)

        return redirect(url_for('user_page', nickname=owner.nickname))

    return render_template("user.htm", user=user,
                           owner=owner, posts=posts_full,
                           rights=rights, time=datetime.utcnow(),
                           owners=owners, members=members, admins=admins, members_ban=members_ban)


@app.route("/_like")
@login_required
def like():
    for_post_id = request.args.get("for_post_id", 0, type=int)
    from_user_id = request.args.get("from_user_id", 0, type=int)
    vote_type = request.args.get("vote_type", 0, type=str)
    for_or_against = 1 if request.args.get("type", 0, type=str) == "for" else -1
    print ("TYPE", for_or_against)
    post = Post.query.filter_by(id=for_post_id).first()
    if vote_type == "zero":
        print "delete"
        like = Post_like.query.filter_by(from_user_id=from_user_id, for_post_id=for_post_id).first()
        post.rating += for_or_against
        like.type = 0
        like.timestamp = datetime.utcnow()
        db.session.add(like)
        db.session.add(post)
        db.session.commit()
    elif vote_type == "down":
        print "add"
        like = Post_like(for_post_id=for_post_id, from_user_id=from_user_id, type=for_or_against) if\
            Post_like.query.filter_by(for_post_id=for_post_id, from_user_id=from_user_id).first() is None\
            else Post_like.query.filter_by(for_post_id=for_post_id, from_user_id=from_user_id).first()
        post.rating += for_or_against
        like.timestamp = datetime.utcnow()
        like.type = for_or_against
        db.session.add(post)
        db.session.add(like)
        db.session.commit()
    return jsonify(result=0)


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route("/_send_rights")
def send_rights():
    user_id = request.args.get("member_id", 0, type=int)
    owner_id = request.args.get("user_id", 0, type=int)
    is_admin = request.args.get('is_admin', 0, type=int)
    if is_admin:
        new_admin = User_follower.query.filter_by(following_id=owner_id, follower_id=user_id).first()
        new_admin.rights = 1
        db.session.add(new_admin)
        db.session.commit()
        return jsonify(result=1)
    else:
        new_follower = User_follower.query.filter_by(follower_id=user_id, following_id=owner_id).first()
        new_follower.rights = 2
        db.session.add(new_follower)
        db.session.commit()
        return jsonify(result=0)


@app.route("/_delete_post")
def delete_post():
    post_id = request.args.get("post_id", 0, type=int)
    post = Post.query.filter_by(id=post_id).first()
    db.session.delete(post)
    db.session.commit()
    return jsonify(result=0)


@app.route("/_send_ban")
def send_ban():
    user_id = request.args.get("member_id", 0, type=int)
    owner_id = request.args.get("user_id", 0, type=int)
    is_ban = request.args.get('is_ban', 0, type=int)
    if is_ban:
        new_ban = User_follower.query.filter_by(following_id=owner_id, follower_id=user_id).first()
        new_ban.banned = 1
        db.session.add(new_ban)
        db.session.commit()
        return jsonify(result=1)
    else:
        new_ban = User_follower.query.filter_by(follower_id=user_id, following_id=owner_id).first()
        new_ban.banned = 0
        db.session.add(new_ban)
        db.session.commit()
        return jsonify(result=0)


@app.route("/_follow_user")
def follovify():
    user_id = request.args.get("view_user_id", 0, type=int)
    owner_id = request.args.get("user_id", 0, type=int)
    is_follower = request.args.get('is_follower', 0, type=int)
    if is_follower:
        new_follower = User_follower(following_id=owner_id, follower_id=user_id, banned=0, rights=2)
        db.session.add(new_follower)
        db.session.commit()
        return jsonify(result=1)
    else:
        new_follower = User_follower.query.filter_by(follower_id=user_id, following_id=owner_id).first()
        db.session.delete(new_follower)
        db.session.commit()
        return jsonify(result=0)


@app.before_request
def before_request():
    g.user = current_user
    # return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
