# -*- coding: utf-8 -*-

from app import db
from datetime import datetime


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    full_name = db.Column(db.String(64), index=True)
    birthday = db.Column(db.String(11))
    vk = db.Column(db.String(120))
    fb = db.Column(db.String(120))
    instagram = db.Column(db.String(120))
    gender = db.Column(db.String(10))
    avatar = db.Column(db.String(120))
    avatar_mini = db.Column(db.String(120), default="/static/img/user-avatar.jpg")
    description = db.Column(db.String(240))
    timestamp = db.Column(db.DATETIME)

    type = db.Column(db.SmallInteger, default=0) # 0- user 1 - blog
    access_rating = db.Column(db.Integer, default=0)
    owner_id = db.Column(db.Integer)

    login = db.relationship('User_login', lazy='dynamic')

    def __init__(self, nickname, full_name, birthday, vk,
                 fb, instagram, gender, avatar,
                 description, type, access_rating, owner_id):
        self.nickname = nickname
        self.full_name = full_name
        self.birthday = birthday
        self.vk = vk
        self.fb = fb
        self.instagram = instagram
        self.gender = gender
        self.avatar = avatar
        self.description = description
        self.timestamp = datetime.utcnow()
        self.type = type
        self.access_rating = access_rating
        self.owner_id = owner_id

    def is_active(self):
        return True

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False



class User_login(db.Model):
    email = db.Column(db.String(64), primary_key=True)
    password = db.Column(db.String(120))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, email, password, user_id):
        self.email = email
        self.password = password
        self.user_id = user_id


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(4000))
    timestamp = db.Column(db.DATETIME)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    image = db.Column(db.String(120))
    rating = db.Column(db.Integer, default=0)
    subuser_id = db.Column(db.Integer)
    comments = db.relationship('Comment', lazy='dynamic')

    def __init__(self, body, user_id, image, subuser_id):
        self.body = body
        self.user_id = user_id
        self.image = image
        self.subuser_id = subuser_id
        self.timestamp = datetime.utcnow()


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(400))
    timestamp = db.Column(db.DATETIME)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))

    def __init__(self, body, user_id, post_id):
        self.body = body
        self.timestamp = datetime.utcnow()
        self.user_id = user_id
        self.post_id = post_id


class User_follower(db.Model): #or blog
    id = db.Column(db.Integer, primary_key=True)
    follower_id = db.Column(db.Integer)
    following_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # blog_id
    banned = db.Column(db.SmallInteger, default=0)  # 0 - unban, 1 - ban
    rights = db.Column(db.SmallInteger, default=2)  # 0 - ADMIN 1 - admin 2 - member (read)

    def __init__(self, follower_id, following_id, banned, rights):
        self.follower_id = follower_id
        self.following_id = following_id
        self.banned = banned
        self.rights = rights


class Blog_admin(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    blog_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, user_id, blog_id):
        self.user_id = user_id
        self.blog_id = blog_id


class Post_like(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    for_post_id = db.Column(db.Integer)
    from_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    type = db.Column(db.Integer) # -1 or 1
    timestamp = db.Column(db.DATETIME)

    def __init__(self, for_post_id, from_user_id, type):
        self.for_post_id = for_post_id
        self.from_user_id = from_user_id
        self.timestamp = datetime.utcnow()
        self.type = type


class User_like(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    for_user_id = db.Column(db.Integer)
    from_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.DATETIME)

    def __init__(self, for_user_id, from_user_id):
        self.for_user_id = for_user_id
        self.from_user_id = from_user_id
        self.timestamp = datetime.utcnow()




# class Blog(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(64), index=True, unique=True)
#     avatar = db.Column(db.String(120))
#     avatar_mini = db.Column(db.String(120))
#     description = db.Column(db.String(240))
#     access_rating = db.Column(db.Integer)
#     owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
#
#
# class Blog_post(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     body = db.Column(db.String(141))
#     timestamp = db.Column(db.DATETIME)
#     rating = db.Column(db.Integer)
#     blog_id = db.Column(db.Integer, db.ForeignKey('blog.id'))
#     comments = db.relationship('Comment', lazy='dynamic')




